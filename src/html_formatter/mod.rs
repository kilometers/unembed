//! Formats `PostContent` into HTML

use downloaders::{Platform, PostContent};
use htmlescape::{decode_html, encode_minimal};
use regex::Regex;
use time::{strftime, strptime};

pub fn format_tweet(post_content: PostContent) -> String {
    let name = post_content.name;
    let handle = post_content.username;
    let content = encode_minimal(&post_content.content);
    let url = post_content.url;
    let date = strptime(&post_content.date.to_string(), "%s").unwrap();
    let date = strftime("%Y-%m-%d", &date).unwrap();

    let link_re = Regex::new(r"(?P<link>(((https://)?([a-zA-Z]{1,32}\.)?[-a-zA-Z0-9_]{1,256})\.[a-zA-Z]{1,32}(/)?[a-zA-Z0-9_%&\?=\-+/]+))").unwrap();
    let handle_re = Regex::new(r"(^| )@(?P<username>([a-zA-Z0-9_]+))").unwrap();
    let hashtag_re = Regex::new(r"(^| )#(?P<hashtag>([a-zA-Z_][a-zA-Z0-9_]+))").unwrap();
    let space_normalizer_re = Regex::new(r"  +").unwrap();

    let content = link_re.replace_all(&content, " <a href='$link'>$link</a>");
    let content = handle_re.replace_all(
        &content,
        " <a href='https://twitter.com/$username'>@$username</a>",
    );
    let content = hashtag_re.replace_all(
        &content,
        " <a href='https://twitter.com/hashtag/$hashtag'>#$hashtag</a>",
    );
    let content = space_normalizer_re.replace_all(&content, " ");

    format!(
        include_str!("twitter_template.html"),
        name = name,
        handle = handle,
        content = content,
        date = date,
        url = url
    )
}

pub fn format_reddit_comment(post_content: PostContent) -> String {
    if let Platform::RedditComment((score, parent_post)) = post_content.platform {
        let username = post_content.username;
        let url = post_content.url;
        let content = decode_html(&post_content.content).unwrap();
        let subreddit = parent_post.subreddit;
        let post_link = parent_post.url;
        let post_title = parent_post.title;
        let post_username = parent_post.username;

        let date = strptime(&post_content.date.to_string(), "%s").unwrap();
        let post_date = strptime(&parent_post.date.to_string(), "%s").unwrap();
        let date = strftime("%Y-%m-%d", &date).unwrap();
        let post_date = strftime("%Y-%m-%d", &post_date).unwrap();

        format!(
            include_str!("reddit_template.html"),
            subreddit = subreddit,
            comment_username = username,
            comment_link = url,
            comment_content = content,
            comment_date = date,
            comment_score = score,
            op_username = post_username,
            op_link = post_link,
            op_title = post_title,
            op_date = post_date,
        )
    } else {
        "so error".to_string()
    }
}
