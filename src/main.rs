//! The purpsoe of unembed is to easily create embedable HTML from links.
//! Using the built in ones from web services is potentially dangerous for user privacy.

#[macro_use]
extern crate clap;
extern crate reqwest;
extern crate time;
#[macro_use]
extern crate failure;
extern crate base64;
extern crate htmlescape;
extern crate hyper;
extern crate regex;
extern crate serde_json;

use regex::Regex;

pub mod downloaders;
pub mod html_formatter;

fn main() {
    let matches = clap_app!(unembed =>
        (version: "0.1")
        (author: "kilometers <kil0meters@pm.me>")
        (about: "Converts Tweets, Facebook posts, and Reddit comments to local HTML to protect reader's privacy.")
        (@arg INPUT:  +required "URL of the post to embed")
        (@arg style: -d --disable_inlnie_style "Disables inline styling")
    ).get_matches();

    let url = matches.value_of("INPUT").unwrap();
    println!("URL: {}", url);

    let post_content = match detect_service(url) {
        "twitter" => {
            let post_content = downloaders::twitter::twitter_downloader(url).unwrap();
            Some(html_formatter::format_tweet(post_content))
        }
        "reddit" => {
            let post_content = downloaders::reddit::reddit_downloader(url).unwrap();
            Some(html_formatter::format_reddit_comment(post_content))
        }
        "facebook" => None,
        _ => {
            eprintln!("INVALID URL");
            None
        }
    };
    if let Some(post_content) = post_content {
        println!("{}", post_content);
    }
}

/// Uses regex to test for the various URL schemas of the websites.
fn detect_service(url: &str) -> &str {
    let twitter_re = Regex::new(
        r"(?i)(https?://(www\.)?(mobile\.)?(m\.)?twitter\.com/[a-zA-Z0-9_]+/status/[0-9]+(\?.+)?)",
    ).unwrap();
    if twitter_re.is_match(url) {
        return "twitter";
    }
    let reddit_re = Regex::new(
        r"(?i)(https?://([a-z]+\.)reddit.com/r/([a-z\.]+)/comments/([a-z0-9]+)/([a-z0-9_]+)/([a-z0-9]+)/?)",
    ).unwrap();
    if reddit_re.is_match(url) {
        return "reddit";
    }
    let facebook_re = Regex::new(r"(?i)(https?://www\.facebook\.com/(([a-z\._]+)/posts/([0-9]+))?(photo\.php\?fbid=([0-9]+))?)").unwrap();
    if facebook_re.is_match(url) {
        return "facebook";
    }
    "None"
}
