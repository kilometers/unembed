use std::fmt;

pub mod reddit;
pub mod twitter;

/// Common struct returned by all `downloader` functions
///
/// ```
/// struct PostConent {
///     name: String,       // Name of the poster
///     username: String,   // Username of the poster
///     dates: [u32],       // Epochs of edits to post
///     content: String,    // The content of the post
///     url: String,        // URL of the post
///     platform: Platform, // The platform of the post (e.g. Reddit)
/// }
/// ```
/// # Examples
/// ```
/// let post_content = PostConent::new("kil0meters", "1", "Hello world", false, Platform::Reddit);
///
/// println!("{}", post_content.to_html());
/// ```
#[derive(Debug)]
pub struct PostContent {
    pub name: String,
    pub username: String,
    pub date: u64,
    pub content: String,
    pub url: String,
    pub platform: Platform,
}

impl PostContent {
    pub fn new(
        name: String,
        username: String,
        date: u64,
        content: String,
        url: String,
        platform: Platform,
    ) -> PostContent {
        PostContent {
            name,
            username,
            date,
            content,
            url,
            platform,
        }
    }
    pub fn get_retweets_and_likes(&self) -> Option<(u64, u64)> {
        let platform = &self.platform;
        if let Platform::Tweet(retweets_and_likes) = platform {
            return Some((retweets_and_likes[0], retweets_and_likes[1]));
        }
        None
    }
}

impl fmt::Display for PostContent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

/// pub enum Platform {
///    Tweet([u64;2]),                    // First value is retweets, second is likes
///    RedditComment((i64, PostContent)), // Also stores the parent post
///    FacebookPost                       //
/// }
#[derive(Debug)]
pub enum Platform {
    Tweet([u64; 2]),
    RedditComment((i64, reddit::RedditPost)),
    FacebookPost,
}
