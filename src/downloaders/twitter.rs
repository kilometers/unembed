//! Twitter API Frontend

use base64;
use downloaders::{Platform, PostContent};
use failure::Error;
use reqwest;
use reqwest::header::{Authorization, ContentType, Headers, UserAgent};
use reqwest::mime;
use serde_json::{self, Value};
use time::strptime;

const TWITTER_API_KEY: &str = "ZHfvlqnH4EZ6S04BalL6UW1zE";
const TWITTER_API_SECRET: &str = "2LBG5BFttO9wdPHpc37MkAfSBlmXvF6B1Jer4ZtPAjB1bmjX7d";

/// Returns access token to authenticate API requests.
/// For more information, see the [Twitter API Documentation](https://developer.twitter.com/en/docs/basics/authentication/overview/application-only).
fn generate_access_token() -> Result<String, Error> {
    let bearer_key =
        base64::encode(format!("{}:{}", TWITTER_API_KEY, TWITTER_API_SECRET).as_bytes());
    let bearer_key = format!("Basic {}", bearer_key);

    let content_type: mime::Mime = "application/x-www-form-urlencoded;charset=UTF-8".parse()?;
    let mut headers = Headers::new();
    headers.set(UserAgent::new("unembed"));
    headers.set(Authorization(bearer_key.to_owned()));
    headers.set(ContentType(content_type));

    let client = reqwest::Client::new();
    let text = client
        .post("https://api.twitter.com/oauth2/token")
        .headers(headers)
        .body("grant_type=client_credentials")
        .send()?
        .text()?;
    let json: Value = serde_json::from_str(&text)?;
    if let Value::String(ref access_token) = json["access_token"] {
        return Ok(format!("Bearer {}", access_token.to_string()));
    };
    Err(format_err!("Invalid Response"))
}

fn get_post_content_from_twitter_response_json(
    text: &str,
    url: &str,
) -> Result<PostContent, Error> {
    let json: Value = serde_json::from_str(&text)?;
    let name = &json["user"]["name"].as_str().unwrap();
    let username = &json["user"]["screen_name"].as_str().unwrap();
    let content = &json["full_text"].as_str().unwrap();
    let time = &json["created_at"].as_str().unwrap();
    let retweets = &json["retweet_count"].as_u64().unwrap();
    let likes = &json["favorite_count"].as_u64().unwrap();

    let platform = Platform::Tweet([retweets.to_owned(), likes.to_owned()]);

    // By default, Twitter uses the format Mon Jan 01 12:00:00 +0000 2000; we need to convert it
    // to a unix time stamp.
    let date = strptime(time, "%a %b %d %H:%M:%S %z %Y")?.to_timespec().sec;

    Ok(PostContent::new(
        name.to_string(),
        username.to_string(),
        date as u64,
        content.to_string(),
        url.to_string(),
        platform,
    ))
}

/// Returns information from a Tweet link.
/// For more information, see the [Twitter API Documentation](https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-show-id).
pub fn twitter_downloader(url: &str) -> Result<PostContent, Error> {
    let access_token = generate_access_token()?;

    let mut headers = Headers::new();
    headers.set(UserAgent::new("unembed"));
    headers.set(Authorization(access_token));

    let url_split: Vec<&str> = url.split('/').collect();
    let id = if url_split[url_split.len() - 1] == "" {
        url_split[url_split.len() - 2]
    } else {
        url_split[url_split.len() - 1]
    };
    let request = format!(
        "https://api.twitter.com/1.1/statuses/show.json?id={}&tweet_mode=extended",
        id
    );

    let client = reqwest::Client::new();
    let text = client.get(&request).headers(headers).send()?.text()?;
    let post_content = get_post_content_from_twitter_response_json(&text, url)?;
    Ok(post_content)
}

#[cfg(test)]
mod twitter_tests {
    use downloaders::twitter::twitter_downloader;
    use downloaders::{Platform, PostContent};
    #[test]
    fn verify_post_just_text() {
        let post_content = twitter_downloader(
            "https://twitter.com/iamdevloper/status/1004621732811083776",
        ).unwrap();
        let (retweets, likes) = post_content.get_retweets_and_likes().unwrap();
        // use likes and retweets from content since that will change
        let post_content_eval = PostContent::new(
            "I Am Devloper".to_string(),
            "iamdevloper".to_string(),
            1528355460,
            "It’s never been harder to be a parody account.".to_string(),
            "https://twitter.com/iamdevloper/status/1004621732811083776".to_string(),
            Platform::Tweet([retweets, likes]),
        );
        assert_eq!(post_content.to_string(), post_content_eval.to_string());
    }
}
