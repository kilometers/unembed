use failure::Error;
use reqwest;
use reqwest::header::{Headers, UserAgent};
use serde_json;

use downloaders::{Platform, PostContent};

#[derive(Debug)]
pub struct RedditPost {
    pub username: String,
    pub url: String,
    pub subreddit: String,
    pub title: String,
    pub date: u64,
}
impl RedditPost {
    pub fn new(
        username: String,
        url: String,
        subreddit: String,
        title: String,
        date: u64,
    ) -> RedditPost {
        RedditPost {
            username,
            url,
            subreddit,
            title,
            date,
        }
    }
}

fn genereate_post_content_from_json(text: &str) -> Result<PostContent, Error> {
    let json: serde_json::Value = serde_json::from_str(&text)?;

    let platform = {
        let post_username = json[0]["data"]["children"][0]["data"]["author"]
            .as_str()
            .unwrap()
            .to_string();
        let post_url = json[0]["data"]["children"][0]["data"]["permalink"]
            .as_str()
            .unwrap()
            .to_string();
        let post_subreddit = json[0]["data"]["children"][0]["data"]["subreddit_name_prefixed"]
            .as_str()
            .unwrap()
            .to_string();
        let post_title = json[0]["data"]["children"][0]["data"]["title"]
            .as_str()
            .unwrap()
            .to_string();
        let post_date = json[0]["data"]["children"][0]["data"]["created_utc"]
            .as_f64()
            .unwrap()
            .round();

        let parent_post = RedditPost::new(
            post_username,
            post_url,
            post_subreddit,
            post_title,
            post_date as u64,
        );

        let upvotes = json[1]["data"]["children"][0]["data"]["ups"]
            .as_f64()
            .unwrap();

        Platform::RedditComment((upvotes as i64, parent_post))
    };

    let username = json[1]["data"]["children"][0]["data"]["author"]
        .as_str()
        .unwrap()
        .to_string();
    let url = json[1]["data"]["children"][0]["data"]["permalink"]
        .as_str()
        .unwrap()
        .to_string();
    let date = json[1]["data"]["children"][0]["data"]["created_utc"]
        .as_f64()
        .unwrap()
        .round();
    let content = json[1]["data"]["children"][0]["data"]["body_html"] // preformatted markdown
        .as_str()
        .unwrap()
        .to_string();

    Ok(PostContent::new(
        "".to_string(),
        username,
        date as u64,
        content,
        url,
        platform,
    ))
}

/// Returns JSON information about a Reddit comment or post.
/// On Reddit, if you add `.json` to the end of a url, it gives you JSON
/// information about the comment. This uses that to return a PostConent struct.
///
/// Note that you have to use a common user agent such as
/// `Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3409.2 Safari/537.36`
/// or use an API key otherwise the Reddit API will block you.
pub fn reddit_downloader(url: &str) -> Result<PostContent, Error> {
    let request = if url.chars().nth(url.len() - 1).unwrap() == '/' {
        format!("{}.json", url)
    } else {
        format!("{}/.json", url)
    };
    let mut headers = Headers::new();
    headers.set(UserAgent::new("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3409.2 Safari/537.36"));

    let client = reqwest::Client::new();
    let text = client.get(&request).headers(headers).send()?.text()?;

    let post_content = genereate_post_content_from_json(&text)?;
    Ok(post_content)
}
